(function(app) {
  app.AppComponent =
    ng.core.Component({
      selector: 'my-app',
      template: '<h1>{{title}}</h1>'
    })
    .Class({
      constructor: function() {
        title = "Hello word";
      }
    });
})(window.app || (window.app = {}));
